# Replay_LSL

Replay recorded LSL (Lab Streaming Layer) samples from XDF files. Essentially XDF to LSL. 

Tested with *Python 2.7* & *3.6*

---
**Currently working with ~10ms accuracy loss of samples**

---

## Install

Install python LSL bindings & numpy. 
```bash
pip install --user pylsl numpy
```

Clone repo
```bash
git clone https://gitlab.com/MisterEz/Replay_LSL.git
cd LSL_Replay
```

Get python XDF library -- currently not on PyPI
```bash
git clone https://github.com/sccn/xdf
mv xdf/Python/xdf.py .
rm -rf xdf
```


## Usage
```bash
python replay_lsl.py <my_sample>.xdf

# For more information of availiable arguments
python replay_lsl.py --help
```
